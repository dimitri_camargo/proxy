/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.Scanner;

/**
 *
 * @author aluno.redes
 */
public class Proxy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*Scanner sc = new Scanner(System.in);
        System.out.println("wich site do you want to go?:");
        String op = sc.next();
        InternetProxy obj = new InternetProxy();
        obj.getResource(op);*/
        
        Browser firefox = new Browser();
        firefox.sendRequest();
    }
    
}
