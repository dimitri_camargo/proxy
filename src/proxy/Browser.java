/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import javax.swing.JOptionPane;

/**
 *
 * @author aluno.redes
 */
public class Browser {
    //send request to ISP
    public void sendRequest(){
        String site = JOptionPane.showInputDialog("enter URL:");
        String response = this.getInternetServiceProvider().getResource(site);
        this.loadResponse(response);
    }
    
    
    private void loadResponse(String response) {
        //rendering page
        System.out.println(response);
    }

    //get the network settings for ISP use
    private ISP getInternetServiceProvider(){
        //getting by proxy
        return new InternetProxy();
    }

    
    
    
}
